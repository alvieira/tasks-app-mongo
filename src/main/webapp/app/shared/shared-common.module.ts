import { NgModule } from '@angular/core';

import { TasksappmongoSharedLibsModule, JhiAlertComponent, JhiAlertErrorComponent } from './';

@NgModule({
    imports: [TasksappmongoSharedLibsModule],
    declarations: [JhiAlertComponent, JhiAlertErrorComponent],
    exports: [TasksappmongoSharedLibsModule, JhiAlertComponent, JhiAlertErrorComponent]
})
export class TasksappmongoSharedCommonModule {}
